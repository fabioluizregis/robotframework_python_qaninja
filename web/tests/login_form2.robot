*** Settings ***
Resource        base.robot

Test Setup      Nova sessão
Test Teardown   Encerra sessão

*** Test Cases ***
Login com sucesso
    Go To           ${url}/login
    Login With      stark       jarvis!
    
    Should Contain Login Alert      Olá, Tony Stark. Você acessou a área logada!

Senha inválida
    [tags]              login_error
    Go To               ${url}/login
    Login With          stark       abc123

    Should Contain Login Alert      Senha é invalida!

Usuário não existe
    [tags]              login_error
    Go To               ${url}/login
    Login With          fabiolr     abc123

    Should Contain Login Alert      O usuário informado não está cadastrado!

*** Keywords ***
Login With
    [Arguments]     ${uname}    ${pass}

    Input Text      css:input[name=username]        ${uname}
    Input Text      css:input[name=password]        ${pass}
    Click Element   class:btn-login

Should Contain Login Alert
    [Arguments]     ${mensagem_esperada}
    ${message}=         Get WebElement      id:flash
    Should Contain      ${message.text}     ${mensagem_esperada}
