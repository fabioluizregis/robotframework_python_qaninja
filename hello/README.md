# RobotFramework_Python_QANinja

Curso "Robot Beginer" - Utilizando o Robot Framework com Python - Na QA Ninja

1 - Ter Python e PIP instalados
2 - Instalar o Robot ( c:\pip install robotframework )

Para rodar o teste inicial --> c:\robot test.robot

3 - pip install robotframework-seleniumlibrary

Rodar o teste web --> c:\robot -d ./log title.robot

Rodar com a tag ironman --> c:\robot -d ./log -i ironman .\checkbox.robot
